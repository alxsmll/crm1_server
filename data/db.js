import mongoose from "mongoose";


export async function connect(){
    try {
        await mongoose.connect('mongodb://localhost/clientes',{useNewUrlParser: true})
        
        console.log('Mongo conectado!! :D');
    } catch(e) {
        console.log('algo salio mal');
        console.log(e);
    }
};