import Cliente from './models/clienteModel';

export const resolvers = {
    Query: {
        getClientesLimit: (root, {limite}) => {
            return Cliente.find().limit(limite)
        },
        getClientes: () => {
            return Cliente.find();
       },
        getCliente: (root, {id}) => {
            return Cliente.findById(id);
       }
    },
    Mutation: {
        async crearCliente(_, {input}){
            const nuevoCliente = new Cliente(input)
            await nuevoCliente.save();
            return nuevoCliente;
        },
        async actualizarCliente(_, {input}){
            return await Cliente.findByIdAndUpdate(id, input, {new: true});
        },
        async eliminarCliente(_, {input}){
            return await Cliente.findByIdAndDelete(id);
        }
    }
}

export default resolvers;