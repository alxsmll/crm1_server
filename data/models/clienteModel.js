import { Schema, model } from "mongoose";

const clienteSchema = new Schema({
        nombre : String,
        apellido: String,
        empresa: String,
        email: String,
        edad: Number,
        tipo: String,
        pedidos : {
                producto: String,
                precio: Number
        }
});

export default model('Cliente', clienteSchema);