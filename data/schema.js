import { makeExecutableSchema } from "graphql-tools";
import { resolvers } from "./resolvers";

const typeDefs = `
type Query {
    getClientesLimit(limite: Int): [Cliente]
    getClientes: [Cliente]
    getCliente(id: ID): Cliente
}
type Cliente{
    id: ID
    nombre: String
    apellido: String
    empresa: String
    email: String
    edad: Int
    tipo: TipoCliente
    pedidos: [Pedido]
}
type Email{
    email: String
}
input EmailInput{
    email: String
}
type Pedido{
    producto: String
    precio: Int
}
input PedidoInput{
    producto: String
    precio: Int
}
""" Campos para los clientes nuevos """
input ClienteInput{
    id: ID
    nombre: String!
    apellido: String!
    empresa: String!
    email: String
    edad: Int!
    tipo: TipoCliente!
    pedidos: [PedidoInput]
} 
""" Asigna la categoria del cliente """
enum TipoCliente{
    BASICO
    PREMIUM
}
""" Mutations para crear nuevos clientes """
type Mutation{
    """ Permite crear nuevos clientes """
    crearCliente(input: ClienteInput): Cliente
    actualizarCliente(input: ClienteInput): Cliente
    eliminarCliente(id: ID! ): String
}
`;

export {typeDefs};

export default makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers
})