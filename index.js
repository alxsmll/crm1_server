import typedefs from './data/schema';
import express from 'express';
import {ApolloServer} from 'apollo-server-express';
import { connect } from "./data/db";
import {resolvers} from "./data/resolvers";

connect();
const app = express();
const server = new ApolloServer({
    schema: typedefs, 
    resolvers: resolvers
});

server.applyMiddleware({app});

app.listen(8080 , () => console.log('El servidor esta funcionando HTTP://localhost:8080/graphql'));